# TXSiri -- the greatest mind in the universe

## Task description

The task was to develop an expert system which can guess user's objects.
The ability to accumulate new information and export data stored in the system into a graphical format was also a necessary requirement.

## Interaction with a system

The program welcomes the user and suggests to guess an object.
The user guesses the object and TxSiri starts to ask questions about the object on which the user must answer "yes" or "no".
This functionality is similar to popular game [Akinator](https://en.akinator.com/).

In the end if TxSiri guessed the object then TxSiri wins otherwise TxSiri suggests the user to input object's name and it's unique quality.
So, TxSiri remembers a new object and in the future games TxSiri can guess it.

Also TxSiri can define objects which are stored in its database.

TxSiri can guess an object to allow a user to guess it by its definition.

In any moment the user can ask TxSiri to print the database and TxSiri will do it in graphic format.

## Project relevance

Nowadays expert systems are increasingly in demand for many practical tasks to automate routine processes.
For example, it medicine such systems help diagnose symptoms of illness without a doctor.

## What's inside?

A binary tree was chosen as the data structure to store the information.

## Platform

This program can only be compiled and run on Windows OC.
This is due to the fact that TxSiri says the all inputed phrases and this function works through system calls in Windows.
Also TxSiri uses TXLib library which also uses WinAPI.

## Results

This project was developed in August 2015.
The code size is 1 KLOC on C++.
